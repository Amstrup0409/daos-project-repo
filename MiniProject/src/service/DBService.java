package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBService {

    static final Connection minConnection = ServerService.loadServer();
    static final Statement  stmt          = ServerService.loadConnection();

    public static void saveToDB() {

        String sql = "select s.name Name, e.name Education from students s join educations e on e.id = s.education_id";

        try {
            ResultSet res = stmt.executeQuery(sql);
            while (res.next()) {

                System.out.println("Name: " + res.getString("Name") + ", "
                        + "Education: " + res.getString("education"));

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static boolean sqlExecute(String sql) {
        try {
            boolean res = stmt.execute(sql);
            return res;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    public static int getId(String name, Object o) {
        String pH = o.getClass().getSimpleName().toLowerCase();

        int ID = -1;
        String sql = "select id from " + pH + "s WHERE name = '" + name + "'";

        try {
            ResultSet res = stmt.executeQuery(sql);
            while (res.next()) {
                ID = res.getInt("ID");

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ID;
    }

}
