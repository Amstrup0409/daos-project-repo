package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Exam;
import model.Grade;
import model.Student;
import storage.Storage;

public class Service {

    public static boolean addGrade(Student s, Exam e, int grade) {
        Connection minConnect = Storage.minConnection;
        String sql = "insert into grades (exam_id, education_id, student_id, grade) values (?,?,?,?)";
        try {
            PreparedStatement prestmt = minConnect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prestmt.clearParameters();
            prestmt.setInt(1, e.getId());
            prestmt.setInt(2, s.getEducation().getId());
            prestmt.setInt(3, s.getId());
            prestmt.setInt(4, grade);

            int affectedRows = prestmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException();
            }

            Grade g = new Grade(0, s.getEducation(), e, s, grade);
            try (ResultSet res = prestmt.getGeneratedKeys()) {
                if (res.next()) {
                    g.setId(res.getInt(1));

                } else {
                    throw new SQLException();
                }

            }

        } catch (SQLException e1) {
            return false;
        }

        return true;

    }

}
