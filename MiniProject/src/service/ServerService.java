package service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ServerService {

    static Connection       minConnection;
    static final Properties prop = loadConfig();

    public static Connection loadServer() {

        String DB_HOST = "localhost";
        String DB_NAME = prop.getProperty("DB_Name");
        String DB_USER = prop.getProperty("DB_User");
        String DB_PASS = prop.getProperty("DB_Password");

        String connString = "jdbc:sqlserver://" + DB_HOST;
        connString += ";databaseName=" + DB_NAME;
        connString += ";user=" + DB_USER;
        connString += ";password=" + DB_PASS + ";";

        try {
            minConnection = DriverManager.getConnection(connString);
        } catch (Exception e) {
            System.out.println("Fejl: " + e.getMessage());
        }
        return minConnection;

    }

    public static Statement loadConnection() {
        try {
            return minConnection.createStatement();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static Properties loadConfig() {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("config.properties"));
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return prop;
    }

}