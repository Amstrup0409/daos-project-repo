package gui;

import java.sql.SQLException;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Education;
import model.Grade;
import model.Student;
import storage.Storage;

public class MainApp extends Application {
    static Storage storage = new Storage();

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws SQLException {
        storage.init();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("DAOS Education Project");
        GridPane pane = new GridPane();
        initContent(pane);
        stage.setWidth(700);
        stage.setHeight(520);
        stage.setResizable(false);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    // -------------------------------------------------------------------------

    private Label               lblEducations, lblStudents, lblGrade;
    private ListView<Education> lvwEducations;
    private ListView<Student>   lvwStudents;
    private ListView<Grade>     lvwGrades;
    private Button              btnAdd;

    private void initContent(GridPane pane) {
        pane.setPrefHeight(650);
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(15);
        pane.setVgap(15);
        pane.setStyle("-fx-background-color: ALICEBLUE");

        lblEducations = new Label("Educations");
        pane.add(lblEducations, 0, 0);

        lvwEducations = new ListView<>();
        lvwEducations.getItems().setAll(storage.getEducations());
        pane.add(lvwEducations, 0, 1);
        ChangeListener<Education> listener = (ov, oldEducation, newEducation) -> updateEducationControls();
        lvwEducations.getSelectionModel().selectedItemProperty().addListener(listener);

        lblStudents = new Label("Students");
        pane.add(lblStudents, 1, 0);

        lvwStudents = new ListView<>();
        pane.add(lvwStudents, 1, 1);
        ChangeListener<Student> listenerStudent = (ov, oldStudent, newStudent) -> updateStudentControls();
        lvwStudents.getSelectionModel().selectedItemProperty().addListener(listenerStudent);

        lblGrade = new Label("Grade");
        pane.add(lblGrade, 2, 0);

        lvwGrades = new ListView<>();
        pane.add(lvwGrades, 2, 1);

        btnAdd = new Button("Add Grade");

        pane.add(btnAdd, 0, 2);

        btnAdd.setOnAction(event -> addAction());

        // ----------------- Methods ------------------------

    }

    public void updateEducationControls() {
        Education education = lvwEducations.getSelectionModel().getSelectedItem();
        if (education != null) {
            lvwStudents.getItems().setAll(education.getStudents());

        }
    }

    public void updateStudentControls() {
        Student s = lvwStudents.getSelectionModel().getSelectedItem();
        if (s != null) {
            lvwGrades.getItems().setAll(s.getGrades());
        }
    }

    public void addAction() {
        Student s = lvwStudents.getSelectionModel().getSelectedItem();
        if (s == null) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("No Students selected");
            a.show();

        } else {
            AddWindow add = new AddWindow("Add Grade", s);
            add.showAndWait();
            updateStudentControls();
        }
    }

}
