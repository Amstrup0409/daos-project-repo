package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Exam;
import model.Student;
import service.Service;

public class AddWindow extends Stage {

    private Student s;

    public AddWindow(String title, Student s) {
        this.s = s;
        initStyle(StageStyle.UTILITY);
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);

        setTitle(title);
        GridPane pane = new GridPane();
        initContent(pane);

        Scene scene = new Scene(pane);
        setScene(scene);

    }

    // -------------------------------------------------------------------------
    private String[]       lblList;
    private TextField      txfName, txfEducation, txfGrade;
    private ListView<Exam> lvwExams;
    private Button         btnOk, btnCancel;

    private void initContent(GridPane pane) {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        lblList = new String[] { "Name:", "Education", "Exam", "Grade" };

        for (int i = 0; i < lblList.length; i++) {
            Label lblFields = new Label(lblList[i]);
            pane.add(lblFields, 0, i);
        }

        txfName = new TextField();
        txfName.setText(s.getName());
        txfName.setEditable(false);
        pane.add(txfName, 1, 0);

        txfEducation = new TextField();
        txfEducation.setText(s.getEducation().getName());
        txfEducation.setEditable(false);
        pane.add(txfEducation, 1, 1);

        lvwExams = new ListView<>();
        lvwExams.getItems().setAll(s.getEducation().getExams());
        pane.add(lvwExams, 1, 2);

        txfGrade = new TextField();
        pane.add(txfGrade, 1, 3);

        btnCancel = new Button("Cancel");
        btnOk = new Button("Ok");

        pane.add(btnCancel, 0, 4);
        pane.add(btnOk, 1, 4);

        btnOk.setOnAction(event ->

        okAction());
        btnCancel.setOnAction(event -> cancelAction());

    }

    private void okAction() {
        Exam e = lvwExams.getSelectionModel().getSelectedItem();
        int g = Integer.parseInt(txfGrade.getText());
        boolean gradeAdded = Service.addGrade(s, e, g);
        if (gradeAdded) {
            hide();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Student already passed this exam");
            alert.initModality(Modality.WINDOW_MODAL);
            alert.initOwner(this);
            alert.show();
        }

    }

    private void cancelAction() {
        hide();
    }

}
