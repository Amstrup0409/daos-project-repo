package model;

import java.sql.Date;

public class Exam {

    private int       id;
    private String    name;
    private Education education;
    private Date      date;

    public Exam(int id, String name, Education education, Date date) {
        this.id = id;
        this.name = name;
        this.education = education;
        this.date = date;
        education.addExam(this);
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Education getEducation() {
        return this.education;
    }

    public Date getDate() {
        return this.date;
    }

    @Override
    public String toString() {
        return name;
    }

}
