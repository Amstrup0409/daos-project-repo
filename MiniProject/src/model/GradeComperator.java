package model;

import java.util.Comparator;

public class GradeComperator implements Comparator<Grade> {

    @Override
    public int compare(Grade o1, Grade o2) {
        return o1.getExam().getName().compareTo(o2.getExam().getName());
    }

}
