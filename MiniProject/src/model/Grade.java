package model;

import java.text.SimpleDateFormat;

public class Grade {

    private int       id;
    private Education education;
    private Exam      exam;
    private Student   student;
    private int       grade;

    public Grade(int id, Education e, Exam exam, Student s, int grade) {
        this.id = id;
        this.education = e;
        this.exam = exam;
        this.student = s;
        this.grade = grade;
        s.addGrade(this);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public Education getEducation() {
        return this.education;
    }

    public Exam getExam() {
        return this.exam;
    }

    public Student getStudent() {
        return this.student;
    }

    public int getGrade() {
        return this.grade;
    }

    @Override
    public String toString() {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = DATE_FORMAT.format(exam.getDate());
        return exam.getName() + ", Grade: " + grade + ". Date: " + dateString;
    }

}
