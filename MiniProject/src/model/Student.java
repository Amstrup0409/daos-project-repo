package model;

import java.util.ArrayList;

public class Student {

    private int              id;
    private String           name;
    private Education        education;
    private ArrayList<Grade> grades = new ArrayList<>();

    public Student(int id, String name, Education education) {
        this.id = id;
        this.name = name;
        this.education = education;
        education.addStudent(this);
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Education getEducation() {
        return education;
    }

    public void addGrade(Grade g) {
        this.grades.add(g);
    }

    public ArrayList<Grade> getGrades() {
        ArrayList<Grade> sorted = new ArrayList<>(this.grades);
        sorted.sort(new GradeComperator());
        return sorted;
    }

    @Override
    public String toString() {
        return "#" + this.id + " - " + this.name + " - " + this.education;
    }

}
