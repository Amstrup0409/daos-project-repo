package model;

import java.util.ArrayList;

public class Education {

    private String name;
    private int    id;
    private ArrayList<Student> students = new ArrayList<>();
    private ArrayList<Exam> exams = new ArrayList<>();

    public Education(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
    
    public void addStudent(Student s) {
        this.students.add(s);
    }
    
    public ArrayList<Student> getStudents() {
        return this.students;
    }
    
    public void addExam(Exam e) {
        this.exams.add(e);
    }
    
    public ArrayList<Exam> getExams() {
        return this.exams;
    }
    
    @Override
    public String toString() {
        return this.name;
    }

}
