package storage;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Education;
import model.Exam;
import model.Grade;
import model.Student;
import service.ServerService;

public class Storage {

    private ArrayList<Education>   educations    = new ArrayList<>();
    private ArrayList<Student>     students      = new ArrayList<>();
    private ArrayList<Exam>        exams         = new ArrayList<>();
    private ArrayList<Grade>       grades        = new ArrayList<>();

    public static final Connection minConnection = ServerService.loadServer();

    public void init() throws SQLException {

        // Load educations
        String sql = "SELECT * FROM educations";
        Statement stmt = minConnection.createStatement();
        ResultSet result = stmt.executeQuery(sql);
        while (result.next()) {
            int id = result.getInt("id");
            String name = result.getString("name");
            this.educations.add(new Education(id, name));
        }

        // Load students
        sql = "SELECT * FROM students";
        stmt = minConnection.createStatement();
        result = stmt.executeQuery(sql);
        while (result.next()) {
            int id = result.getInt("id");
            String name = result.getString("name");
            Education education = getEducationByID(result.getInt("education_id"));
            this.students.add(new Student(id, name, education));
        }

        // Load exams
        sql = "SELECT * FROM exams";
        stmt = minConnection.createStatement();
        result = stmt.executeQuery(sql);
        while (result.next()) {
            int id = result.getInt("id");
            String name = result.getString("name");
            Education education = getEducationByID(result.getInt("education_id"));
            Date date = result.getDate("date");
            this.exams.add(new Exam(id, name, education, date));
        }

        // Load grades
        sql = "SELECT * FROM grades";
        stmt = minConnection.createStatement();
        result = stmt.executeQuery(sql);
        while (result.next()) {
            int id = result.getInt("id");
            Education education = getEducationByID(result.getInt("education_id"));
            Exam exam = getExamByID(result.getInt("exam_id"));
            Student student = getStudentByID(result.getInt("student_id"));
            int grade = result.getInt("grade");
            this.grades.add(new Grade(id, education, exam, student, grade));
        }

    }

    public ArrayList<Education> getEducations() {
        return this.educations;
    }

    public Education getEducationByID(int id) {
        for (Education e : this.educations) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public ArrayList<Student> getStudents() {
        return this.students;
    }

    public Student getStudentByID(int id) {
        for (Student s : this.students) {
            if (s.getId() == id) {
                return s;
            }
        }
        return null;
    }

    public ArrayList<Exam> getExams() {
        return this.exams;
    }

    public Exam getExamByID(int id) {
        for (Exam e : this.exams) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public ArrayList<Grade> getGrades() {
        return this.grades;
    }
}
